@extends('layouts/contentLayoutMaster')
@section('title', 'Appointment')
@section('vendor-style')
<link rel="stylesheet" href="{{ asset(('app-assets/vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
<div class="col-md-12 col-lg-12 col-xl-12">
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Schedule Timings</h4>
                    <div class="profile-box" data-select2-id="5">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Timing Slot Duration</label>
                                    <select class="select form-control select2-hidden-accessible time_intervals_slots" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option data-select2-id="9" value="15">15 mins</option>
                                        <option data-select2-id="3" value="30"> 30 mins</option>
                                        <option data-select2-id="11" value="60">1 Hour</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card schedule-widget mb-0">
                                    
                                    <!-- Schedule Header -->
                                    <div class="schedule-header">
                                        
                                        <!-- Schedule Nav -->
                                        <div class="schedule-nav">
                                            <ul class="nav nav-tabs nav-justified">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#slot_sunday">Sunday</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#slot_monday">Monday</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#slot_tuesday">Tuesday</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#slot_wednesday">Wednesday</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#slot_thursday">Thursday</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#slot_friday">Friday</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#slot_saturday">Saturday</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /Schedule Nav -->
                                        
                                    </div>
                                    <!-- /Schedule Header -->
                                    
                                    <!-- Schedule Content -->
                                    <div class="tab-content schedule-cont">
                                        
                                        <!-- Sunday Slot -->
                                        <div id="slot_sunday" class="tab-pane fade active show">
                                            <h4 class="card-title d-flex justify-content-between">
                                            <span>Time Slots</span>
                                            <a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
                                            </h4>
                                            <p class="text-muted mb-0">Not Available</p>
                                        </div>
                                        <!-- /Sunday Slot -->
                                        <!-- Monday Slot -->
                                        <div id="slot_monday" class="tab-pane fade">
                                            <h4 class="card-title d-flex justify-content-between">
                                            <span>Time Slots</span>
                                             <a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
                                            </h4>
                                            <!-- Slot List -->
                                            @if(count($monday) > 0 )
                                            @foreach($monday as $time)
                                            <div class="chip chip-danger mr-1">
                                                <div class="chip-body">
                                                    <span class="chip-text">{{ $time->start_time }} - {{ $time->end_time }}</span>
                                                    <div class="chip-closeable">
                                                        <i class="feather icon-x"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @else
                                            <p class="text-muted mb-0">Not Available</p>
                                            @endif
                                            <!-- /Slot List -->
                                        </div>
                                        <!-- /Monday Slot -->
                                        <!-- Tuesday Slot -->
                                        <div id="slot_tuesday" class="tab-pane fade">
                                            <h4 class="card-title d-flex justify-content-between">
                                            <span>Time Slots</span>
                                            <a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
                                            </h4>
                                            <p class="text-muted mb-0">Not Available</p>
                                        </div>
                                        <!-- /Tuesday Slot -->
                                        <!-- Wednesday Slot -->
                                        <div id="slot_wednesday" class="tab-pane fade">
                                            <h4 class="card-title d-flex justify-content-between">
                                            <span>Time Slots</span>
                                            <a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
                                            </h4>
                                            <p class="text-muted mb-0">Not Available</p>
                                        </div>
                                        <!-- /Wednesday Slot -->
                                        <!-- Thursday Slot -->
                                        <div id="slot_thursday" class="tab-pane fade">
                                            <h4 class="card-title d-flex justify-content-between">
                                            <span>Time Slots</span>
                                            <a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
                                            </h4>
                                            <p class="text-muted mb-0">Not Available</p>
                                        </div>
                                        <!-- /Thursday Slot -->
                                        <!-- Friday Slot -->
                                        <div id="slot_friday" class="tab-pane fade">
                                            <h4 class="card-title d-flex justify-content-between">
                                            <span>Time Slots</span>
                                            <a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
                                            </h4>
                                            <p class="text-muted mb-0">Not Available</p>
                                        </div>
                                        <!-- /Friday Slot -->
                                        <!-- Saturday Slot -->
                                        <div id="slot_saturday" class="tab-pane fade">
                                            <h4 class="card-title d-flex justify-content-between">
                                            <span>Time Slots</span>
                                            <a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
                                            </h4>
                                            <p class="text-muted mb-0">Not Available</p>
                                        </div>
                                        <!-- /Saturday Slot -->
                                    </div>
                                    <!-- /Schedule Content -->
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<!-- Add Time Slot Modal -->
<div class="modal fade custom-modal" id="add_time_slot">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Time Slots</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('appointment.store') }}"  method="post">
                    @csrf
                    <div class="hours-info">
                        <div class="row form-row hours-cont">
                        </div>
                    </div>
                    
                    <div class="add-more mb-3">
                        <a href="javascript:void(0);" class="add-hours"><i class="feather icon-plus-square"></i> Add More</a>
                    </div>
                    <div class="submit-section text-center">
                        <button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Time Slot Modal -->
@endsection
@section('page-script')
<script src="{{ asset('pages-Js/appointment/index.js') }}"></script>
@endsection