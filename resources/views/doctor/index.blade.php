@extends('layouts/contentLayoutMaster')
@section('title', 'Doctor')
@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(('app-assets/vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
 <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <!-- <h4 class="card-title">Zero configuration</h4> -->
                                </div>
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table doctor_table">
                                                <thead>
                                                    <tr>
                                                        <th>Sr.No</th>
                                                        <th>Name</th>
                                                        <th>Phone No</th>
                                                        <th>Email</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   <tr>
                                                       
                                                   </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
@endsection
@section('vendor-script')
{{-- vendor files --}}
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection
@section('page-script')
<script src="{{ asset('pages-Js/doctor/index.js') }}"></script>
@endsection