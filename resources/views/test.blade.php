@extends('layout.index')
@section('title', 'test')
@section('content')
<div class="col-md-12 col-lg-12 col-xl-12">
	
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Schedule Timings</h4>
					<div class="profile-box" data-select2-id="5">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label>Timing Slot Duration</label>
									<select class="select form-control select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
										<option data-select2-id="8">Choose an Option</option>
										<option data-select2-id="9">15 mins</option>
										<option data-select2-id="3">30 mins</option>
										<option data-select2-id="10">45 mins</option>
										<option data-select2-id="11">1 Hour</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="card schedule-widget mb-0">
									
									<!-- Schedule Header -->
									<div class="schedule-header">
										
										<!-- Schedule Nav -->
										<div class="schedule-nav">
											<ul class="nav nav-tabs nav-justified">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#slot_sunday">Sunday</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#slot_monday">Monday</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#slot_tuesday">Tuesday</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#slot_wednesday">Wednesday</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#slot_thursday">Thursday</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#slot_friday">Friday</a>
												</li>
												<li class="nav-item">
													<a class="nav-link " data-toggle="tab" href="#slot_saturday">Saturday</a>
												</li>
											</ul>
										</div>
										<!-- /Schedule Nav -->
										
									</div>
									<!-- /Schedule Header -->
									
									<!-- Schedule Content -->
									<div class="tab-content schedule-cont">
										
										<!-- Sunday Slot -->
										<div id="slot_sunday" class="tab-pane fade active show">
											<h4 class="card-title d-flex justify-content-between">
											<span>Time Slots</span>
											<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
											</h4>
											<p class="text-muted mb-0">Not Available</p>
										</div>
										<!-- /Sunday Slot -->
										<!-- Monday Slot -->
										<div id="slot_monday" class="tab-pane fade">
											<h4 class="card-title d-flex justify-content-between">
											<span>Time Slots</span>
											<a class="edit-link" data-toggle="modal" href="#edit_time_slot"><i class="feather icon-edit mr-1"></i>Edit</a>
											</h4>
											
											<!-- Slot List -->
											
											<div class="chip chip-danger mr-1">
												<div class="chip-body">
													<span class="chip-text">9:00 pm - 11:30 pm</span>
													<div class="chip-closeable">
														<i class="feather icon-x"></i>
													</div>
												</div>
											</div>
											<div class="chip chip-danger mr-1">
												<div class="chip-body">
													<span class="chip-text">8:00 pm - 8:30 pm</span>
													<div class="chip-closeable">
														<i class="feather icon-x"></i>
													</div>
												</div>
											</div>
											<div class="chip chip-danger mr-1">
												<div class="chip-body">
													<span class="chip-text">8:00 pm - 11:30 pm</span>
													<div class="chip-closeable">
														<i class="feather icon-x"></i>
													</div>
												</div>
											</div>
											<div class="chip chip-danger mr-1">
												<div class="chip-body">
													<span class="chip-text">8:00 pm - 11:30 pm</span>
													<div class="chip-closeable">
														<i class="feather icon-x"></i>
													</div>
												</div>
											</div>

											<!-- /Slot List -->
										</div>
										<!-- /Monday Slot -->
										<!-- Tuesday Slot -->
										<div id="slot_tuesday" class="tab-pane fade">
											<h4 class="card-title d-flex justify-content-between">
											<span>Time Slots</span>
											<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
											</h4>
											<p class="text-muted mb-0">Not Available</p>
										</div>
										<!-- /Tuesday Slot -->
										<!-- Wednesday Slot -->
										<div id="slot_wednesday" class="tab-pane fade">
											<h4 class="card-title d-flex justify-content-between">
											<span>Time Slots</span>
											<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
											</h4>
											<p class="text-muted mb-0">Not Available</p>
										</div>
										<!-- /Wednesday Slot -->
										<!-- Thursday Slot -->
										<div id="slot_thursday" class="tab-pane fade">
											<h4 class="card-title d-flex justify-content-between">
											<span>Time Slots</span>
											<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
											</h4>
											<p class="text-muted mb-0">Not Available</p>
										</div>
										<!-- /Thursday Slot -->
										<!-- Friday Slot -->
										<div id="slot_friday" class="tab-pane fade">
											<h4 class="card-title d-flex justify-content-between">
											<span>Time Slots</span>
											<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
											</h4>
											<p class="text-muted mb-0">Not Available</p>
										</div>
										<!-- /Friday Slot -->
										<!-- Saturday Slot -->
										<div id="slot_saturday" class="tab-pane fade">
											<h4 class="card-title d-flex justify-content-between">
											<span>Time Slots</span>
											<a class="edit-link" data-toggle="modal" href="#add_time_slot"><i class="feather icon-plus-square"></i> Add Slot</a>
											</h4>
											<p class="text-muted mb-0">Not Available</p>
										</div>
										<!-- /Saturday Slot -->
									</div>
									<!-- /Schedule Content -->
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<!-- Add Time Slot Modal -->
<div class="modal fade custom-modal" id="add_time_slot">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Time Slots</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="hours-info">
						<div class="row form-row hours-cont">
							<div class="col-12 col-md-10">
								<div class="row form-row">
									<div class="col-12 col-md-6">
										<div class="form-group">
											<label>Start Time</label>
											<select class="form-control">
												<option>Choose an Option</option>
												<option>12.00 am</option>
												<option>12.30 am</option>
												<option>1.00 am</option>
												<option>1.30 am</option>
											</select>
										</div>
									</div>
									<div class="col-12 col-md-6">
										<div class="form-group">
											<label>End Time</label>
											<select class="form-control">
												<option>Choose an Option</option>
												<option>12.00 am</option>
												<option>12.30 am</option>
												<option>1.00 am</option>
												<option>1.30 am</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="add-more mb-3">
						<a href="javascript:void(0);" class="add-hours"><i class="feather icon-plus-square"></i> Add More</a>
					</div>
					<div class="submit-section text-center">
						<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /Add Time Slot Modal -->
@endsection
@push('scripts')
<script type="text/javascript">
	$( document ).ready(function() {
		$('#monday').change(function() {
			var interval = $('#interval :selected').val();
	if ($(this).prop('checked')) {
		var test = "";
		var i;
				for (i = 1; i <= 24/interval ; i++) {
				test +=  i+"helloo<br>";
				}
		$('#mondaySpan').html(test);
	}
	else {
		$('#mondaySpan').html('');
	}
	});
		$(".add-hours").on('click', function () {
		
		var hourscontent = '<div class="row form-row hours-cont">' +
				'<div class="col-12 col-md-10">' +
						'<div class="row form-row">' +
								'<div class="col-12 col-md-6">' +
										'<div class="form-group">' +
												'<label>Start Time</label>' +
												'<select class="form-control">' +
														'<option>Choose an Option</option>' +
														'<option>12.00 am</option>' +
														'<option>12.30 am</option>' +
														'<option>1.00 am</option>' +
														'<option>1.30 am</option>' +
												'</select>' +
										'</div>' +
								'</div>' +
								'<div class="col-12 col-md-6">' +
										'<div class="form-group">' +
												'<label>End Time</label>' +
												'<select class="form-control">' +
														'<option>Choose an Option</option>' +
														'<option>12.00 am</option>' +
														'<option>12.30 am</option>' +
														'<option>1.00 am</option>' +
														'<option>1.30 am</option>' +
												'</select>' +
										'</div>' +
								'</div>' +
						'</div>' +
				'</div>' +
				'<div class="col-12 col-md-2"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger"><i class="feather icon-trash-2"></i></a></div>' +
		'</div>';
		
$(".hours-info").append(hourscontent);
return false;
});
	});
</script>
@endpush