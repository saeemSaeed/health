<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Model\Doctor::class, function (Faker $faker) {
    return [
            'first_name'=> $faker->name,
            'last_name'=> $faker->name,
            'email'=> $faker->unique()->safeEmail,
            'password'=> Hash::make("password"),

            'phone_number'=>$faker->phoneNumber,
            'country'=>$faker->phoneNumber,
            'state'=>$faker->state,
            'city'=>$faker->city,
            'gender'=>rand(0,1),
            'address'=>$faker->address,
            'type'=>rand(0,1),
            'info'=>$faker->paragraph,
    ];
});
