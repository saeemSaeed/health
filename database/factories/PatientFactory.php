<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Patient::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
      'password'=> \Illuminate\Support\Facades\Hash::make("password"),
        'phone_number' => $faker->phoneNumber,
        'country' => $faker->phoneNumber,
        'state' => $faker->state,
        'city' => $faker->city,
        'gender' => rand(0,1),
        'address' => $faker->address,

    ];
});
