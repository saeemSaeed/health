$(function () {
	var dataThumbView = $(".doctor_table").DataTable({
		responsive: false,
		processing: true,
		serverSide: true,
		ajax: "/doctor",
		columns: [
			{
				data: 'id',
				name: 'id', 
			
			},
			{
				data: 'name',
				name: 'name', 
			
			},
			{
				data: 'phone_number',
				name: 'phone_number', 
			
			},
			{
				data: 'email',
				name: 'email', 
			
			},
			{
				data: 'action',
				name: 'action', 
			
			},
	],
	order: [] ,	
	});

});
