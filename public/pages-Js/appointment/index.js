$( document ).ready(function() {



  //   $('.chip-closeable').on('click', function () {
  //       alert();
  //   // $(this).closest('.chip').remove();
  // })
var interval = $( ".time_intervals_slots option:selected" ).val();;

$('.time_intervals_slots').on('change', function() {
    interval = this.value;
});

$(".add-hours").on('click', function () {
var hourscontent = '<div class="row form-row hours-cont">' +
    '<div class="col-12 col-md-10">' +
        '<div class="row form-row">' +
            '<div class="col-12 col-md-6">' +
                '<div class="form-group">' +
                    '<label>Start Time</label>' +
                    '<select class="form-control" name="start_time[]" required>' +
                        '<option>Choose an Option</option>' +
                            GenTimePullDown(parseInt(interval))+
                    '</select>' +
                '</div>' +
            '</div>' +
            '<div class="col-12 col-md-6">' +
                '<div class="form-group">' +
                    '<label>End Time</label>' +
                    '<select class="form-control" name="end_time[]" required>' +
                        '<option>Choose an Option</option>' +
                            GenTimePullDown(parseInt(interval))+
                    '</select>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>' +
    '<div class="col-12 col-md-2"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger"><i class="feather icon-trash-2"></i></a></div>' +
'</div>';

$(".hours-info").append(hourscontent);
return false;
});


function GenTimePullDown(MinuteInterval) {
    var o, n, i, g, r, S, l;
    o = "<option value='00:00'>12:00 AM</option>", n = 0, i = 0, r = 0, S = 0, g = 0, l = "AM";
    for (var e = 1; e < 60 / MinuteInterval * 24; e++) 60 == (i += MinuteInterval) && (g = n += 1, n >= 12 && (l = "PM"), i = 0), r = n > 12 ? n - 12 : n, 0 == n && (r = 12), 1 == n.toString().length && (g = "0" + n.toString(), g = "0" + n.toString()), 1 == r.toString().length && (r = "0" + r.toString()), S = 1 == i.toString().length ? "0" + i.toString() : i, o = o + "\n<option value='" + (g.toString() + ":" + S.toString()+ " " + l) + "' >" + (r.toString() + ":" + S.toString() + " " + l) + "</option>";
    return o;
}


$(document).on("click", ".add_time_slot_modal_button", function () {
     var myBookId = $(this).data('id');
     $(".modal-body #week_day").val( myBookId );
});




$(document).on('click', '.action-delete', function(e) {    
    var target = $(this);   
        e.stopPropagation();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'get',
                    url: '/appointment/delete/'+$(this).attr("data-id"),
                    success: function(data) {
                    target.closest('.chip').remove();

                        $.blockUI({
                            message: '<div class="spinner-border text-white" role="status"></div>',
                            css: {
                                backgroundColor: 'transparent',
                                border: '0'
                            },
                            overlayCSS: {
                                opacity: 0.5
                            }
                        });

                        Swal.fire('Deleted!', 'Appointment Slot has been deleted.', 'success')

                        $.unblockUI();
                    },


                    error: function(data){
                        $.unblockUI();
                    }
                });
            }
        })
    });
   




});