<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Doctor extends Authenticatable
{
  use Notifiable;

  protected $guard = 'doctor';
    protected $fillable=[
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'country',
        'state',
        'city',
        'gender',
        'address',
        'type',
        'info',
    ];
  protected $hidden = [
    'password', 'remember_token',
  ];
}
