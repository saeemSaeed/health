<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use App\Model\Appointment;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{

    public function index()
    {
        $sunday = Appointment::where('doctor_id',1)->where('day_id',config('constants.weekDays.sunday'))->get();
        $monday = Appointment::where('doctor_id',1)->where('day_id',config('constants.weekDays.monday'))->get();
        $tuesday = Appointment::where('doctor_id',1)->where('day_id',config('constants.weekDays.tuesday'))->get();
        $wednesday = Appointment::where('doctor_id',1)->where('day_id',config('constants.weekDays.wednesday'))->get();
        $thursday = Appointment::where('doctor_id',1)->where('day_id',config('constants.weekDays.thursday'))->get();
        $friday = Appointment::where('doctor_id',1)->where('day_id',config('constants.weekDays.friday'))->get();
        $saturday = Appointment::where('doctor_id',1)->where('day_id',config('constants.weekDays.saturday'))->get();


    	return view('appointment.index',compact('sunday','monday','tuesday','wednesday','thursday','friday','saturday'));
    }

    public function store(Request $request) 
    {
    	foreach ($request->start_time as $index=>$data){

    		Appointment::create([
    			'doctor_id'	 	=>	1,
    			'day_id'	 	=>	$request->day_id,
    			'start_time'	=>	$data,
    			'end_time'	 	=>	$request->end_time[$index],
    		]);

    	}
    	return redirect()->back();
    }

    public function destroy(Appointment $appointment)
    {
        $appointment->delete();
        return response()->json(['success' => true], 200);
    }
}
